package S1;

public class I extends H {
    private long t;
    private K k;

    public void f() {

    }
}

class H {
    public float n;

    public void f(int g) {

    }
}

class J {
    public void i(I l) {

    }
}

class K {
    private L l;
    private M m;

    public K(L l) {
        this.l = l;
        m = new M();
    }
}

class L {
    public void metA() {

    }
}

class M {
    public void metB() {

    }
}
